import React, { Component } from 'react'
import axios from 'axios'

import { url } from '../components/App'

const withUpdateFileDialog = WrappedComponent => {
  class WithUpdateFileDialog extends Component {
    constructor(props) {
      super(props)

      this.state = {
        hiddenDialog: true,
        selectedFile: null,
        newFileName: '',
        savingFile: false,
      }

      this.selectFile = this.selectFile.bind(this)
      this.closeDialog = this.closeDialog.bind(this)
      this.updateFile = this.updateFile.bind(this)
      this.changeFileName = this.changeFileName.bind(this)
    }

    closeDialog() {
      this.setState({ hiddenDialog: true });
    }

    updateFile(e) {
      e.preventDefault()
      const { newFileName, fileId } = this.state
      this.setState({ savingFile: true }, () => {
        axios.post(`${url}/file/${fileId}/rename`, {
          filename: newFileName
        })
          .then(response => {
            this.setState({ savingFile: false, hiddenDialog: true }, () => {
              this.props.onPageChange(this.props.currentPage)
            })
          })
      })
    }

    selectFile(selectedFile) {
      this.setState({ selectedFile, hiddenDialog: false, newFileName: selectedFile.name, fileId: selectedFile.id })
    }

    changeFileName(evt) {
      this.setState({ newFileName: evt.target.value })
    }

    render() {
      return (
        <WrappedComponent
          {...this.props}
          {...this.state}
          closeDialog={this.closeDialog}
          updateFile={this.updateFile}
          selectFile={this.selectFile}
          changeFileName={this.changeFileName}
        />
      )
    }
  }

  return WithUpdateFileDialog
}

export default withUpdateFileDialog
