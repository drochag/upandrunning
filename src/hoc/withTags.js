import React, { Component } from 'react'
import axios from 'axios'

import { url } from '../components/App'

const withTags = WrappedComponent => {
  class WithTags extends Component {
    constructor(props) {
      super(props)
      
      this.state = {
        loadingTags: true,
        tags: [],
        tagSearch: '',
      }
  
      this.onFilterTags = this.onFilterTags.bind(this)
    }

    componentDidMount() {
      axios.get(`${url}/tags`)
        .then(tagsResponse => {
          this.setState({ tags: tagsResponse.data, loadingTags: false })
        })
    }
  
    onFilterTags(tagSearch) {
      this.setState({ tagSearch })
    }

    render() {
      return (
        <WrappedComponent
          {...this.props}
          {...this.state}
          onFilterTags={this.onFilterTags}
        />
      )
    }
  }

  return WithTags
}

export default withTags
