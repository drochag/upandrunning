import React, { Fragment } from 'react'
import { Nav } from 'office-ui-fabric-react/lib/Nav';
import { Spinner, SpinnerSize } from 'office-ui-fabric-react/lib/Spinner';
import { SearchBox } from 'office-ui-fabric-react/lib/SearchBox';
import styled from 'styled-components'
import { Link } from 'react-router-dom';

import withTags from '../hoc/withTags'

const StyledSearchBox = styled(SearchBox)`
  margin-top: 20px;
`

const onRenderLink = (props) => {
  if (!props.href) {
    return <h2 style={{
      display: 'block',
      textAlign: 'center',
      paddingLeft: '40px',
      lineHeight: '34px',
      boxSizing: 'border-box',
    }} className={props.className}>{props.children}</h2>
  }

  return <Link className={props.className} to={props.href}>
    {props.children}
  </Link>;
}

const TagsList = ({ tags, tagSearch, loadingTags, onFilterTags }) => loadingTags ? (
  <h1>Loading Tags <Spinner size={SpinnerSize.medium} style={{ display: 'inline-block', marginLeft: 10 }} /></h1>
) : (
  <Fragment>
    <StyledSearchBox
      placeholder="FilterTags"
      onChange={onFilterTags}
    />

    <Nav
      groups={[
        {
          links: [
            {
              isExpanded: true,
              name: 'Tags',
              links: tags.filter(tag => tagSearch === '' || !tagSearch ? true : tag.tag.indexOf(tagSearch) >= 0).map(tag => ({
                name: `${tag.tag} (${tag.files})`,
                url: `/${tag.tag}`,
              })),
            }
          ]
        }
      ]}
      expandedStateText={'expanded'}
      collapsedStateText={'collapsed'}
      linkAs={onRenderLink}
      expandButtonAriaLabel={'Expand or collapse'}
    />
  </Fragment>
)


export default withTags(TagsList)
