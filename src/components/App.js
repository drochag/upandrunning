import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Container } from 'react-grid-system';
import { registerIcons } from '@uifabric/styling';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faChevronUp,
  faChevronDown,
  faChevronLeft,
  faChevronRight,
  faFileAlt,
  faSearch,
  faTimesCircle,
  faAngleDoubleLeft,
  faAngleDoubleRight,
} from '@fortawesome/free-solid-svg-icons'

import MainPage from './MainPage'

library.add(faChevronUp)
library.add(faChevronRight)
library.add(faChevronLeft)
library.add(faChevronDown)
library.add(faAngleDoubleLeft)
library.add(faAngleDoubleRight)
library.add(faFileAlt)
library.add(faSearch)
library.add(faTimesCircle)

registerIcons({
  icons: {
    'chevrondown': <FontAwesomeIcon icon="chevron-down" />,
    'chevronup': <FontAwesomeIcon icon="chevron-up" />,
    'chevronleft': <FontAwesomeIcon icon="chevron-left" size="sm" fixedWidth />,
    'chevronright': <FontAwesomeIcon icon="chevron-right" size="sm" fixedWidth />,
    'doublechevronleft': <FontAwesomeIcon icon="angle-double-left" />,
    'doublechevronright': <FontAwesomeIcon icon="angle-double-right" />,
    'search': <FontAwesomeIcon icon="search" />,
    'clear': <FontAwesomeIcon icon="times-circle" />,
  }
});

function App() {
  return (
    <Router>
      <Container>
        <Route path="/:tag?" exact component={MainPage} />
      </Container>
    </Router>
  );
}

export default App;
export const url = 'http://tim.uardev.com/trial-project/api';
