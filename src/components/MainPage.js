import React, { Component } from 'react'
import axios from 'axios'
import { Container, Row, Col } from 'react-grid-system';
import styled from 'styled-components'

import { FilesList, TagsList } from '.'
import { url } from './App'

const BorderedCol = styled(Col)`
  @media screen and (min-width: 768px) {
    box-shadow: -2px -2px 2px rgba(0,0,0,.2);
  }
`

class MainPage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      loadingFiles: true,
      currentPage: 1,
      selectedTag: this.props.match.params.tag || '',
    }

    this.onPageChange = this.onPageChange.bind(this)
    this.getFilesUrl = this.getFilesUrl.bind(this)
  }

  componentDidMount() {
    axios.get(this.getFilesUrl(null, this.props.match.params.tag))
      .then(filesResponse => {
        const { total_files: totalFiles, files } = filesResponse.data
        this.setState({ loadingFiles: false, files, totalFiles })
      })
  }

  componentDidUpdate() {
    if (this.props.match.params.tag !== this.state.selectedTag) {
      this.setState({ currentPage: 1, selectedTag: this.props.match.params.tag, loadingFiles: true }, () => {
        axios.get(this.getFilesUrl(1, this.props.match.params.tag || false))
          .then(filesResponse => {
            const { total_files: totalFiles, files } = filesResponse.data
            this.setState({ loadingFiles: false, files, totalFiles })
          })
      })
    }
  }

  getFilesUrl(forcedPage, forcedTag) {
    const { selectedTag, currentPage } = this.state
    let filesUrl = `${url}/files?page=${forcedPage ? forcedPage : currentPage}`

    if ((selectedTag || forcedTag) && forcedTag !== false) {
      filesUrl += `&tag=${forcedTag ? forcedTag : selectedTag}`
    }

    return filesUrl
  }

  onPageChange(currentPage) {
    this.setState({ loadingFiles: true, currentPage })
    axios.get(this.getFilesUrl(currentPage))
      .then(filesResponse => {
        const { total_files: totalFiles, files } = filesResponse.data
        this.setState({ loadingFiles: false, files, totalFiles })
      })
  }

  render() {
    const {
      loadingTags,
      loadingFiles,
      tags,
      files,
      totalFiles,
      selectedTag,
      currentPage,
      tagSearch,
    } = this.state
    return (
      <Container>
        <Row>
          <Col md={4}>
            <TagsList
              loadingTags={loadingTags}
              onFilterTags={this.onFilterTags}
              tags={tags}
              tagSearch={tagSearch}
            />
          </Col>
          <BorderedCol md={8}>
            <FilesList
              loadingFiles={loadingFiles}
              selectedTag={selectedTag}
              files={files}
              selectFile={this.selectFile}
              totalFiles={totalFiles}
              currentPage={currentPage}
              onPageChange={this.onPageChange}
            />
          </BorderedCol>
        </Row>
      </Container>
    )
  }
}

export default MainPage
