import React, { Fragment } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Spinner, SpinnerSize } from 'office-ui-fabric-react/lib/Spinner';
import { Row, Col } from 'react-grid-system';
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { DefaultButton, PrimaryButton } from 'office-ui-fabric-react/lib/Button';
import Pagination from 'office-ui-fabric-react-pagination';
import { Dialog, DialogType, DialogFooter } from 'office-ui-fabric-react/lib/Dialog';
import { TextField } from 'office-ui-fabric-react/lib/TextField';

import withUpdateFileDialog from '../hoc/withUpdateFileDialog'

const FileCol = styled(Col)`
  margin: 10px 0;
`

const BackIcon = styled(FontAwesomeIcon)`
  margin-right: 10px;
`

const StyledIcon = styled(BackIcon)`
  margin-bottom: 10px;
  vertical-align: middle;
`

const StyledLink = styled(Link)`
  color: inherit;
  text-decoration: none;
  display: block;
  line-height: 30px;
  width: 100%;
  padding: 0 10px;
`

const StyledButton = styled(DefaultButton)`
  padding: 0;
  margin-right: 10px;
`

const BackButton = styled(StyledButton)`
  vertical-align: middle;
`

const TotalFiles = styled.span`
  font-size: 0.4em;
  vertical-align: super;
`

const FilesList = ({
  files,
  selectedTag,
  totalFiles,
  onPageChange,
  currentPage,
  loadingFiles,

  // from withUpdateFileDialog
  hiddenDialog,
  selectedFile,
  newFileName,
  savingFile,
  selectFile,
  closeDialog,
  updateFile,
  changeFileName,
}) =>
  loadingFiles ? (
    <h1>Loading Files <Spinner size={SpinnerSize.medium} style={{ display: 'inline-block', marginLeft: 10 }} /></h1>
  ) : (
    <Fragment>
      <h1>
        {selectedTag && (
          <BackButton>
            <StyledLink to="/">
              <BackIcon icon="chevron-left" />
              Show all files
            </StyledLink>
          </BackButton>
        )}
        Files List {selectedTag && <span>| {selectedTag}</span>} <TotalFiles>({totalFiles} total files)</TotalFiles></h1>
      <Row>
        {files.map(file => (
          <FileCol key={file.id} sm={6} md={4}>
            <StyledIcon icon="file-alt" size="2x" />
            <StyledButton onClick={() => selectFile(file)}>
              {file.name}
            </StyledButton>
          </FileCol>
        ))}
      </Row>
      {totalFiles > 10 && (
        <Row>
          <Pagination
            currentPage={currentPage}
            totalPages={Math.ceil(totalFiles / 10)}
            onChange={onPageChange}
          />
        </Row>
      )}
      {!hiddenDialog && (
          <Dialog
            hidden={hiddenDialog}
            onDismiss={closeDialog}
            dialogContentProps={{
              type: DialogType.largeHeader,
              title: savingFile ?
                <span>Saving File <Spinner size={SpinnerSize.small} style={{ display: 'inline-block', marginLeft: 10 }} /></span>
                : <span>Edit File - {selectedFile.name}</span>,
            }}
            modalProps={{
              isBlocking: true,
            }}
          >
            <form onSubmit={updateFile}>
              <TextField label="File Name" value={newFileName} onChange={changeFileName} />
            </form>
            <DialogFooter>
              <PrimaryButton onClick={updateFile} text="Save" />
              <DefaultButton onClick={closeDialog} text="Cancel" />
            </DialogFooter>
          </Dialog>
        )}
    </Fragment>
  )

export default withUpdateFileDialog(FilesList)
